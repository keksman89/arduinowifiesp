package developer.ink1804.arduinowifiesp

import com.google.gson.GsonBuilder
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface ArduinoRestClient {

    companion object {
        fun create(url: String, port: String): ArduinoRestClient {
            val logging = HttpLoggingInterceptor()

            val okHttpClient = OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .connectionPool(ConnectionPool(0, 1, TimeUnit.MICROSECONDS))
                    .addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build()

            val builder: Retrofit.Builder = Retrofit.Builder()
                    .baseUrl("http://$url:$port")//local
                    .addConverterFactory(
                            GsonConverterFactory.create(
                                    GsonBuilder()
                                            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create()
                            )
                    )
                    .client(okHttpClient)
            val retrofit = builder.build()

            return retrofit.create(ArduinoRestClient::class.java)
        }
    }

    @GET("/")
    fun pushToken(
            @Query("text") text: String
    ): Call<ResponseBody>


}