package developer.ink1804.arduinowifiesp

interface Logger {
    fun showStatus(data: String)
    fun log(tag: String,text: String)
}