package developer.ink1804.arduinowifiesp

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import android.text.method.ScrollingMovementMethod
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity(), View.OnTouchListener, Logger {

    @SuppressLint("SetTextI18n")
    override fun log(tag: String, text: String) {
        runOnUiThread {
            tvLog.text = "${tvLog.text}${getCurrentTime()} D/$tag: $text\n"
        }
    }

    companion object {
        var direction: Byte = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
    }

    private fun startClient() {
//        ArduinoHttpClient(etPort.text.toString(), etAddress.text.toString(), this)
//        ArduinoUdpClient(this)
        log("Connection", "RequestSend")

        ArduinoRestClient
                .create(etAddress.text.toString(), etPort.text.toString())
                .pushToken("HELLO THERE ALEX").enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        if (response.body() != null) {
                            log("Response", response.body()!!.string())
                        }else {
                            log("Response", "empty response")

                        }
                        log("Connection", "closed")
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        log("Response:", t.message.toString())
                    }

                })
    }

    override fun showStatus(data: String) {
        runOnUiThread {
            tvInfo.text = data
        }
    }

    private fun initView() {
        btUp.setOnTouchListener(this)
        btDown.setOnTouchListener(this)
        btLeft.setOnTouchListener(this)
        btRight.setOnTouchListener(this)

        tvLog.movementMethod = ScrollingMovementMethod()
        btConnect.setOnClickListener { startClient() }
        btShare.setOnClickListener {
            share(tvLog.text.toString())
        }
    }


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        direction = when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                when (v?.id) {
                    R.id.btUp -> 3
                    R.id.btDown -> 4
                    R.id.btLeft -> 1
                    R.id.btRight -> 2
                    else -> 0
                }
            }
            MotionEvent.ACTION_UP -> 100
            else -> direction
        }

        v?.performClick()
        return false
    }

    private fun share(text: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, text)
            type = "text/plain"
        }

        startActivity(sendIntent)
    }

    private fun getCurrentTime(): String {
        val calendar = Calendar.getInstance()
        val sdf = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        return sdf.format(calendar.time)
    }
}
