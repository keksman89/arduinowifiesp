package developer.ink1804.arduinowifiesp

import android.util.Log
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class ArduinoUdpClient(var logger: Logger) : Thread() {

    var i = 0
    var data = byteArrayOf(0)
    var udpPort = 50000
    var address: InetAddress? = null
    var socket: DatagramSocket? = null

    init {
        try {
            socket = DatagramSocket()
            address = InetAddress.getByName(ArduinoHttpClient.hostAddress)
        } catch (e: Exception) {
            Log.wtf("UdpClient", e.toString())
            logger.log("UpdClient", e.message.toString())
        }

        start()
    }

    override fun run() {
        while (true) {
            val temp = MainActivity.direction
            var s = "" + MainActivity.direction
            data = s.toByteArray()

            if (temp.toInt() != 100) {
                val pack = DatagramPacket(data, data.size, address, udpPort)
                try {
                    socket?.send(pack)
                    i = 0

                    Thread.sleep(200)
                } catch (e: Exception) {
                    logger.log("UpdClient", e.message.toString())
                    Log.wtf("UdpClient", e.toString())
                }
            } else {
                if (i == 0) {
                    s = "" + 0

                    data = s.toByteArray()
                    val pack = DatagramPacket(data, data.size, address, udpPort)

                    try {
                        socket?.send(pack)
                        Thread.sleep(200)
                    } catch (e: Exception) {
                        logger.log("UpdClient", e.message.toString())
                        Log.wtf("UdpClient", e.toString())
                    }
                }

                i = 1// stop sending data
            }
        }
    }


}