package developer.ink1804.arduinowifiesp

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.io.PrintWriter
import java.net.Socket


class ArduinoHttpClient(port: String, hostAddress: String, var logger: Logger) : Thread() {

    companion object {
        var hostAddress = "172.20.0.210"
        var port = "59487"
    }

    var greetingsFromS: String? = null

    init {
        ArduinoHttpClient.hostAddress = hostAddress
        ArduinoHttpClient.port = port
        start()
        logger.showStatus("Connecting ...")
        logger.log("HttpClient", "Start connecting to $hostAddress:$port")
    }

    override fun run() {
        try {
            Socket(hostAddress, port.toInt()).use { socket ->
                logger.showStatus("Connected!")
                logger.log("HttpClient", "Connected to $hostAddress:$port")
                val pw = PrintWriter(OutputStreamWriter(socket.getOutputStream()), true)
                val br = BufferedReader(InputStreamReader(socket.getInputStream()))

                logger.log("HttpClient", "data ${br.readText()}")

                greetingsFromS = br.readLine()

                if (greetingsFromS == "ready") {
                    logger.log("HttpClient", "status ready")

                    ArduinoUdpClient(logger)
                }
            }
        } catch (e: Exception) {
            logger.log("HttpClient", "Failed to connect $hostAddress\nReason: ${e.message}")
            logger.showStatus("Connection failed.")
            e.printStackTrace()
        }
    }

}